# Image2Dmm

## What is this?

This is a tool to convert color coded png files into maps that can be loaded by byond's dream maker.
It's main purpose is to make it easier for mappers to quickly iterate on layouts.

Essentially it turns this:

![Before](https://i.imgur.com/iMjWPJB.png)

into this:

![After](https://i.imgur.com/do77BdV.png)

## How do I use it?

`image2dmm -i input.png -o output.dmm`

This will convert the png file into a map using the default `replacements.cfg` file. If you want to know what each color means, just open it up.

## Making your own color schemes

The format of `replacements.cfg` is simple, a hex color code followed by a tile definition. The definitions are lifted straight out of normal dm maps and can and should be copied from there.

If you want to make your own color schemes, create an empty map and fill it with the objects/turfs/areas you want to use. Next, open the map file in a text editor and prepend each tiledef with a hex color like so:

`"a" = (/turf/open/space/basic,/area/space)`

would become

`#ff0000 "a" = (/turf/open/space/basic,/area/space)`

This would replace all red pixels with a basic space turf and a space area.

You can also use `default` instead of a color code to make this replacement be used for any colors not in the list.

To use your own `.cfg` file simply use the `-c` argument:

`image2dmm -i input.png -c config.cfg -o output.dmm`

The map data at the bottom is not parsed by the tool and can be deleted if you want.