#include <iostream>
#include <iomanip>
#include <map>
#include <fstream>
#include <sstream>

#include "fmt/format.h"
#include "lodepng.h"
#include "tclap/CmdLine.h"

std::map<std::string, std::pair<std::string, std::string>> LoadReplacements(const std::string &);

int main(int argc, char** argv) {

    std::string inputPath, outputPath, configPath;

    try {
        TCLAP::CmdLine cmd("Converts a png image into a dmm map file by apply a set of replacement rules.", ' ', "0.2");

        TCLAP::ValueArg<std::string> inputPathArg("i", "input", "Path to png file to convert.", true, {}, "string");
        TCLAP::ValueArg<std::string> outputPathArg("o", "output", "Where to output finished map file.", true, {}, "string");
        TCLAP::ValueArg<std::string> configPathArg("r", "replacements", "The replacement file path.", false, "replacements.cfg", "string");

        cmd.add(inputPathArg);
        cmd.add(outputPathArg);
        cmd.add(configPathArg);

        cmd.parse( argc, argv );

        inputPath = inputPathArg.getValue();
        outputPath = outputPathArg.getValue();
        configPath = configPathArg.getValue();

    } catch(TCLAP::ArgException &e) {
        std::cerr << "error: " << e.error() << " for argument " << e.argId() << std::endl;
    }

    std::map<std::string, std::pair<std::string, std::string>> replacements = LoadReplacements(configPath);

    std::ofstream output(outputPath);

    if(output.fail())
    {
        std::cerr << "Error creating output file " << outputPath << std::endl;
        return 1;
    }

    for(auto const& entry : replacements)
    {
        output << fmt::format("\"{0}\" = {1}", entry.second.first, entry.second.second) << std::endl;
    }

    output << std::endl;
    output << "(1,1,1) = {\"" << std::endl;

    std::vector<unsigned char> image; //the raw pixels
    unsigned width, height; // image sizes

    //decode
    unsigned error = lodepng::decode(image, width, height, inputPath);

    //if there's an error, display it
    if(error)
    {
        std::cout << "decoder error " << error << ": " << lodepng_error_text(error) << std::endl;
        return 1;
    }

    auto it = image.cbegin();
    int col = 0;

    while(it != image.cend())
    {
        auto r = *it++;
        auto g = *it++;
        auto b = *it++;
        it++;
        col++;

        std::string color = fmt::format("#{:02x}{:02x}{:02x}", r, g, b);

        auto findIterator = replacements.find(color);

        if(findIterator == replacements.end())
        {
            if(replacements.find("default") != replacements.end()) {
                output << replacements.at("default").first;
                fmt::print(stdout, "Color {} not found, using default replacement", color);
            } else {
                fmt::print(stderr, "Color {} not found and no replacement defined either.", color);
                return 1;
            }
        } else {
            output << replacements.at(color).first;
        }

        if(col >= width)
        {
            col = 0;
            output << std::endl;
        }
    }

    output << "\"}" << std::endl;
    output.close();

    std::cout << "Done!" << std::endl;
    return 0;
}

std::map<std::string, std::pair<std::string, std::string>> LoadReplacements(const std::string &path) {
    std::map<std::__cxx11::string, std::pair<std::__cxx11::string, std::__cxx11::string>> replacements;

    std::ifstream replacementDefinitions(path);
    std::__cxx11::string line;

    while(getline(replacementDefinitions, line) && !line.empty())
    {
        std::__cxx11::string color, tileKey, tileDefinition;
        std::stringstream lineStream(line);
        lineStream >> color >> tileKey;
        getline(lineStream, tileDefinition);

        tileKey = tileKey.substr(1, tileKey.size() - 2);

        tileDefinition = tileDefinition.substr(tileDefinition.find_first_of('('), tileDefinition.size());

        replacements[color] = make_pair(tileKey, tileDefinition);
    }
    return replacements;
}